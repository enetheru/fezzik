Mon 30 Dec 2019 09:10:20 ACDT
-----------------------------
I messed up and lost the changes to this document from last entry, lucky it was
ony one thing.. but i have no record of my thoughts on it.. and it helped
define the purpose of this git repo.

basically from what i suspect was in it, this repo is about being a
documentation and overview of the whole fezzik project, dealing with mostly
design..

The sorts of designs that i want to add are in the readme but its like
* computer systems design
* network design
* electrical grid design
* interior and exterier aesthetic design
* physical design
* base platform and works
* etc.

its all about the van itself and the large overview of the whole project.

I notice also that this is the project that has all the issues in it.. for
previous ideas and choices that i ahve made.. so it acts as a high level TODO
list too.. it would be nice if that list was easily exportable to a document
rather than stuck on the gitlab servers, because i rarely use the web
interface.. I guess it would be good to use when collaborating, but for a
single user project its not very useful at all.

OK so the diagrams i want to include are at least mostly SVG if exported from
google draw, but the draw.io ones are in some specific format, which means i
have had to export it and its not perfect.. so draw.io is out.. i will have to
continue the work using inkscape instead. or some other vector drawing
application

Thu 02 Jan 2020 10:16:19 ACDT
-----------------------------
Going through the README now and  i want to turn it into a much larger
document, with hostorical things about the van, perhaps a timeline graph of
activity, etc.. i like timelines.

so either i need to make my diary a little more public, or whatever..

still, the readme cant be the place for all the information, so i guess only a
summary of the object, with links to other documents.

How do i want to present those other documents? because its easier to write
hthem with the markup as i go rather than revisit them later to add all the
extra stuff.

OK so i think i have managed to pair down the size of the paragraphs in the
readme, but i really need to get the strucutre of the document down so that i
can spread the information around..

I just spent some time fixing the diagrams after exporting them from third
parties.. sriously they suck at making things work by standards.. so having
them defined independently from the cloud services ends up maing them portable
between viewer.

whats next.. some actual thought and information is needed.

Fri 03 Jan 2020 09:50:12 ACDT
-----------------------------
OK so i figured that I would develop this into a website using gitlab pages for
a bit of fun, or general usefulness.. its something i can enable on my other
sites aswell to make things clearer, and also it has some CI/CD enabled. which
would be ok to add to projects like m2i and such. but perhaps not now.

Given this is the case, perhaps i can start thinking about how i wish to
structure the development of the page, and see what i can make happen. there
are some website generators used in gitlab it might be worth seeing if they
make my life easier or harder. from my viewing it seems kind of useless to use
the templates as they offer a kind of specific thing, and my dreams are unique.

I went and broke my enetheru endpoint. not sure whats going on with that.

anyway, now that i have the pipeline for website development on the
fezzik.enetheru.info domain i can push forward with this.

Sun 05 Jan 2020 12:42:51 ACDT
-----------------------------
OK so website deployment is up, i can serve pages and such, so i need to figure
out a structure of how i wish to display the data, how i wish to present the
documentation, i could use either a premade toolkit like sphinx, doxygen, or
any of the things. but i think i prefer to make it in straight html since it
really is so simple. i just want to work out a structure to align to it.

what about a brainstorm on how it wants to appear.
i have to know what it is first.

fezzik, both documentation and presentation, project management, I want to have
a good landing page with a broschure, but not like the existing cooky cutter
things.

* broschure
* documentation
* presentation - can this be rolled into documentation?
* project management, thats issues and milestones which is taken care of
  already

Whats on the broschure?
* a dream board
* links to social media pages
* friends of the project
* links to the bug tracker, and documentation
* infographic style data both historical and realtime
* Gallery of interesting photos, diplicated to instagram etc.
    WIP photos
* google maps

Whats in the documentation?
* van platform documents and images
* plans to be implemented.

what is the aesthetic design of the broschure, i want it to be like an
infographic, pretty sure this is th design..

so for starters having the cartoon
Created a new document for the broschure

Wed 08 Jan 2020 11:15:52 ACDT
-----------------------------
OK back to this website idea. its definitely something i want to keep moving
forward on. something i need to think about is whether this project is a
website, or whether it simply holds a website, fezzik is the computers brains,
so i guess then it needs to hold the website rather than be the website.

that means i need to change the gitlab website runner.

fuck me i just blew a chunk of time simply updating the structure of the
repository, but at least it makes a little more sense now.

some of the workflow for having the wiki in a submodule is a bit weird.
I'm tempted to remove the blog part and merge that into the diary, since thats
basically the same thing. but i'm not sure as of yet whether thats a good move,
i know for sure that if i add to the blog it will clog up the wiki, so i dont
want to expand on it at all. 

perhaps in the fuure there can b a thing i can add to the page to make it work.
an internal federated blog to thepage.. i need to continue brainstorming the
structure of the repo so that I can start to move forward with the coding.

the re-structure allowed me to get the website for this on my local host at
http://fezzik.local as it was already setup for this purpose.

now i can continue to make it real.

Wed 08 Jan 2020 13:48:26 ACDT
-----------------------------
moved locations. so now its structural.

OK so if i am going to generate this broschure page, whats the first thing i
want to show? since there are only a subset of the items available right now, i
think perhaps a slideshow might be a goo start.

ugh, using other peoples code for something so trivial makes me sick.

OK if i am going to create a slideshow from d3.js what does that involve, a
timer on a class object with multiple pictures it loads up and then go through
them with the little dots being used to change between them, except its not
dots, its anything to control the direction.

OK so it appears that there are no examples, so i will have to make something u
myself.

and i've forgotten all my d3 knowledge, so i guess i will be starting
from scratch

Thu 09 Jan 2020 21:48:06 ACDT
-----------------------------
A rudimentary start has been made on the slideshow, enought to commit to the
repo.

Now i have to give it some content to cycle through, each piece should be at
this stage an image, and a link.
what would have made this more idea is to make it so that it doent depend on
svg and you can have arbitrary html inside the 'page' but thats ok, we can have
arbitrary svg instead, which is basically the same thing.

It would be nice to attach data to it using the classic d3 paradigm. or simply
us the attached data.

Sat 11 Jan 2020 17:16:16 ACDT
-----------------------------
OK so the slideshow has a basic start. i attach data using datum, so tha it can
be used within the module code, it shows pictures, i need to give it real
pictures now?

Added some wiuck placeholder images to test gallery appropriateness and it
helped me figure out that the images should be very plain, and altered using
css styles to make them nice, and have nice titles

but thats all i can get to for now as i have a dinner party to attend in town

Sun 12 Jan 2020 09:23:03 ACDT
-----------------------------
One thing i notice immediattlyafter building this is that on a laptop screen
the slider is huge, because there is no limit to its size and it maintains
aspect ratio. the only way to change that is to create a column in the middle
of the page. but i really dislike that style of web design, and i think its
lazy.

What other way can i limit the size of the slider, but still have it work OK.

The transition could be like a cover art viewer, it could fade in and fade out.
provide a custom transition function?

its all about how the object appears and dissapears and wheter that breaks the
visual paradigm of the page layout. for now though i will ignore it.

I need to visually style the text to make it nicer.

It might be worth making the contents of the svg separately defined and added
to the object.. that way each item can be separately styled on the main page,
is just a matter of how i might go about that.

If i can just add an svg block rather than specify a title, a link, and an
image.. then i can allow it to have wildly different and even animated contents
per 'page' and it wont be some bespoke strict subset of items.

but that would be a fairly hefty alteration, so i think its not important right
now.. what is important is to provide controls to stop and start the
transitions, skip to a specific transition and have the links go places.

even perhaps just having the links go places is most imporatnt right now

Sun 12 Jan 2020 11:03:47 ACDT
-----------------------------
I haev to leave the cafe, too many people and not enogh chairs.
i have almost gotten the basics done, but am having trouble with triggering the
transition due to losing context within an onclick handler

nayway i ill have to solve that problem later.

Mon 13 Jan 2020 13:06:06 ACDT
-----------------------------
the slideshow is now majority complete, and now i have to make it go places. or
at least have all the places that it goes to be active.. and so far thats ok.

I might want to make the WIP gallery my google album..hmm perhaps not. having
it link to my main website would be better with a similar photo viewer, but
with a browser aswell.. will see how i make that happen.

I can see myself using d3 to make a lot more things than this.. anyway, whats
next?

I think documentation? bottom layer? ugh.. who knows.. i have to change my
posture before i get a bad back.

Mon 20 Jan 2020 12:57:35 ACDT
-----------------------------
Looking at the state of the page as it currently stands, lets be super critical
and make a list of things to change.

* So white, need to either embrace the whiteness of the page and make the text
  pretty, or change it
* images and text in the sideshows are bogus, need to be fixed, and font
  replaced
* title of the page is bogus and needs to be changed.
* content of the lists seems weird still.. Needs to have its design changed.

overall I am unhappy with the current aesthetic, I think it needs a
header/body/footer design with nice horizontal lines.

I guess the thing about the title is that If I make it something actually
designed it becomes an actual interesting item on the page, and needs to be
treated with the same respect.. A title and a by line,having it as Fezzik, the
van project, is dumb..

If I call it Fezzik, then nobody will understand, hence the by line. Perhaps 'A
Van Dwelling Project'

* A Van Dwelling Project
* My life in a van
* van dweller experiment
* Van dwelling the final frontier
* 20xx a van dwelling odyssey
* The only winning move is not to play - War games is pretty good.
* To live would be an awfully big adventure
* you went back to the carpet store after beating cancer?, Lame.
* even if the chance of success is small, does that not make it worth trying?

What about the things that I personally believe? My own things rather than
copying someone else.
* A calculated trade of time and money to experiment with an idea.
* Two years is OK to sacrifice to test this out.
* You can do anything, what do you want?
* A life lived avoiding risk, is not a life well lived.
* What the fastest way to independence?
* It technically is a space craft.


Maybe like Oglaf I can have it choose from a list of tag lines.
that would mean that I don't have to choose, and might make the site more
interesting.

Sun 26 Jan 2020 11:02:17 ACDT
-----------------------------
OK whats next on the list of things to do to update this from being a pile of
shit into something nice.

most obvious to me is how ugly it is.I definitely need to split it into three
parts, lets see how i can make that a reality.

OK so I now have three parts to the website, but they still don't look like
three parts. Well sort of.

next is the title, which is butt ugly. I need a futuristic font, and have the
tag line show 'neath it.

so lets find a font that works.

OK made the font for the whole site mono space,and made the title look nicer.
I want to have a little icon of a van on the right hand side inside the title.

Mon 27 Jan 2020 16:41:21 ACDT
-----------------------------
OK I finished with the styling aspects for now since its not a job I enjoy and
I can spend stupid amounts of time obsessing over pointless things. That will
be for later.

I think that since my imagination and what I have planned is in disarray I need
to bring it back by working through the documentation.. Which means really
working through the documentation.

For now I will write here in my diary and see what happens.

I think whatever pops into my head will be a good place to start, so lets now
think about plumbing..

What information do I have on hand? I want Greg viragoes video to show up
There are two main parts to plumbing, liquid based services, and gas based
services.

Liquid:
* Fresh Water
* Grey Water
* Black Water
* liquid fuel like diesel

Gas:
* propane
* compressed air

Liquid includes fresh, grey and black water, could also include diesel for
systems designed to use that but since I have no intention to use such things I
am leaving that out of my research.

Greg Virgo has some great videos on his channel regarding these things.
* Water Services: https://www.youtube.com/watch?v=28VwvLNf2r4
* Water Tanks: https://www.youtube.com/watch?v=3053PsSxqDQ
* Installing LPG: https://www.youtube.com/watch?v=bTK6tk3y9pc
* external hook-ups: https://www.youtube.com/watch?v=a0I7-o6VmbU
* Gas cupboard: https://www.youtube.com/watch?v=zV3J0KNC8YM&t=237s

TO think about what I want to do in terms of plumbing means I need to have a
fixed idea of how I wish the kitchen to operate, whether I need toilets,
shower, gas, blah blah.

OK so for me personally I don't wish to have a toilet inside my vehicle, For
emergencies shitting in a bag in a pot does just fine, and I can throw it out
like dog doodoo. The rest of the time its not hard to use publicly available
toilets. Which means I don't have to worry about black water.

This thread on /a/vandwellers sums up good points regarding throwing away
poop into the trash.
https://www.reddit.com/r/vandwellers/comments/54ghrk/debate_topic_is_putting_your_poop_in_the_trash/

The general gist of the post encase it becomes unavailable in  the future is
that people throw away poo all the time. Disposable nappies, medical waste,
etc. its so common that even if a large number of van dwellers do it its still
pales in comparison to the existing trashing of potentially harmful material
they the trash systems are robust against such things.

So then what remains is fresh and grey water, and low pressure propane for
kitchen and BBQ use.

Since nothing is as yet set in stone regarding internal layout, what I would
like to have is
* under van tanks for fresh and grey water
* High PVC tube tank for solar hot water
* a retractable hose in the kitchen that can reach up to the ceiling for
  indoor showers( with fold up cubicle )
* a hose outlet on the exterior of the van near they doors.
* an accessible drain on the inside to tip things into the grey water
* Hidden fill port for fresh water

I got distracted looking at rack mounted PVC solar hot water set-ups.

for gas
* propane plumbed to kitchen from gas cupboard
* swap and go tank? Or under van tank?
* plumbed to external port for BBQ set-ups
* compressor and tank for compressed air tools, use in water pressurisation,
    tyre inflation, anything else? gass struts for anything scifi'ish?

well the compressed air thing is a bit silly considering I almost never use it
for things.. Might be useful but not sure. Just trying to maximise my options.

well OK, some things depend on others, positioning of the tanks and their
ingress will inform the interior kitchen design and plumbing, so I should
measure out the possible tank positions, figure out how to mount and secure, and plumb them to the interior of the cabin.

There is also the additional consideration on how to monitor their usage, I
will have to figure that out too.

I think minimal plumbing to the grey water is best, which means putting it
directly under the kitchen to have a vertical drop straight into it.
Wed 29 Jan 2020 10:03:40 ACDT
-----------------------------
OK I set the max-width property to 1024 so that the website sits in the middle
of the page and the sideshow doesn't get oversized.
Slowly its coming together after many iterations and fixes.

I'm comfortable with the way the main page looks for now. So I need to move
onto filling out the documentation. Time to copy paste the plumbing data.

Fri 31 Jan 2020 09:29:50 ACDT
-----------------------------
SO I have discovered that the Wiki wont easily link to documents in the
repository, so  have to figure out my data strategy so that I can solve both
having information in the Wiki and also in the website, though perhaps since I
am using the Wiki as my primary documentation it wont matter.

I have plans for the computer systems around here somewhere...

Thu 21 May 2020 21:33:55
-----------------------------
I still haven't implemented much of my plans because they aren't necessary in
the short term. However I have been given another couple of pieces of hardware
which I wish to utilise. An ODroid HC2 and a rpi zero,  the Odroid is yet
another SOC so I have another full hardware device which I have a 1TB SSD
attached to.. So things need to be updated in terms of documentation aswell as
plan for implementation.

I guess my main issue right now is that I have to setup smb filesharing because
its the easiest to use for the variety of devices.

Let's go make that happen for now

As I go I think about whether for this particular server I should set up
ACL's based on request network address, and firewall rules to lock down the
ports.
TODO - add ACL's for smb to Serapeum, or perhaps just use firewall rules
TODO - lock down firewall ports for Serapeum and add openings for SSH, SMB

```bash
$ sudo smbpasswd -a enetheru
```

Added smb username to local host.

OK, so file access works, testing write access OK. but browsing doesnt show the
list of shares.

``` smbclient -L serapeum -U% ``` works, but ```smbtree -b -N``` doesnt.


Learning about things i have previously learned about is weird, like the fact
that discoverabilty is now handles by multicast dns rather than netbios and
smb1 protocol things.. because the latter were depracated and inscure years
ago. so i'm installing avahi now on serapeum

It appears that whilst older protocols are insecure and shouldnt be used, the
avahi/mdns  discovery only shows the server names not the shares, and the file
managers were built with respect to browsing old insecure protocols, so to make
things 'just work' you need to really dumb this shit down to insecure and
horrible... ugh. figuring out the correct way to create an easily discoverable
smb file share is appalling.

OK its a bit of a wierd mix of both zeroconf and shitty smb to get the
discovery experience working well.. which is just gross. but it works so meh.

ugh, and now my power has collapsed again.. I really need to find out why I am
using so much power. I guess the laptop has been going flat chat for a while.
