Fezzik
======
Is the name of the van I have purchased, to build out into a permanent living
space. It's Andre the Giant's character name from 'The Princess Bride'. I'm not
super settled on it though, so meh.

>>>
Inigo Montoya: That Vizzini, he can *fuss*.
Fezzik: Fuss, fuss... I think he like to scream at *us*.
Inigo Montoya: Probably he means no *harm*.
Fezzik: He's really very short on *charm*.
Inigo Montoya: You have a great gift for rhyme.
Fezzik: Yes, yes, some of the time.
Vizzini: Enough of that.
Inigo Montoya: Fezzik, are there rocks ahead?
Fezzik: If there are, we all be dead.
Vizzini: No more rhymes now, I mean it.
Fezzik: Anybody want a peanut?
Vizzini: DYEEAAHHHHHH!
>>>

The fezzik project has been split into multiple sub projects
* hyperion - IoT based sensors and control software
* web-index - personal website project
* network-scraper - local area networks scraper

This repository hosts the display website, high level bug and issue tracking,
documentation in the Wiki,  and is my general purpose management tool.
