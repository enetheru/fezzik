/* It may seem stupid to write my own slide show, but its a valuable learning
 * tool to create seemingly simple things and think about how that might work.

so its a side scrolling paginator that uses the same scemantics as adding axis
to a graph.

# import into project
import * as sh from './app/slideshow.js';

# have slideshow data to join to parent
# format is json and has "title", "img","url"
var slides = [
    { "title":"Issue Board", "img":"./res/sh_issueBoard.jpg",   "url":"https://gitlab.com/enetheru/fezzik/-/boards" },
    { "title":"Dream Board", "img":"./res/sh_dreamGallery.jpg", "url":"https://www.pinterest.com.au/enetheru/" }
];

# Create Slideshow Object
var slideshow = sh.slideshow()
    .width( 1280 )
    .height( 480 )
    .delay( 6000 )
    .transit( 1000 );

#Select container object and call the slideshow object to create it.
d3.select('#slider')
    .datum( slides )
    .call( slideshow );

#TODO Brainstorm
- create default styles for objects, or simply change the data array to be svg drawings themselves.
    - title text
    - image area
        - vignette, rounded corners, etc
- multiple transition styles, left, right up, down, fade, cover browser, etc.
- custom transitions
- previous and next controls
*/
import "/lib/d3.js";

export function slideshow(){
    var width = 1600;
    var height = 900;
    var transit = 1000; //speed of transition in milliseconds
    var delay = 6000; //delay between transitions

    var position = 0;

    let slider = null;
    let controls = null;
    var content = null;

    function slideshow( selection ){
        content = selection.datum();
        position = content.length - 1;

        let svg = selection.append('svg')
            .attr('viewBox', `0 0 ${width} ${height}` );

        slider = svg.append('g')
            .attr('id', 'window' );

        slider.transition()
            .duration( 0 )
            .attr('transform', `translate( -${width}, 0)`)
            .on('start', slide );

        // Control Layer
        // =============
        // Add a row of buttons the length of the array, down in the
        // bottom left corner. when clicked they change the position, and then
        // trigger a transition.
        controls = svg.append('g');

        controls
            .selectAll('rect')
            .data( selection.datum() )
            .enter()
          .append( 'rect' )
            .attr('id', function( d,i ){return i;})
            .attr('fill', 'gray')
            .attr('width', 15 )
            .attr('height', 15)
            .attr('rx', 5 )
            .attr( 'x', function(d,i){
                return (i * 20) + 35;
            } )
            .attr( 'y', `${height -20}` )
            .on('click', function(d,i){
                slideshow.skipTo( i );
            });

        // TODO Add a left and right button, which necessitates a left and right
        // transition

    }

    //Transition function
    function slide(){
        position = ( position += 1 ) % content.length;
        // remove old element outside the screen
        slider.select('#old').remove();
        //reset the positions of the slider group
        slider.attr('transform', null );
        slider.select('#new')
            .attr('transform' , null )
            .attr('id', 'old');

        // Create a new group element to the right to fill with things.
        let g_new = slider.append('g')
          .append('a')
            .attr('href', `${content[position].url}`)
            .attr('id', 'new' )
            .attr('transform', `translate( ${width} ,0 )` );

        g_new.append('mask')
            .attr('id', 'testmask' )
          .append('rect')
            .style('fill','lightblue')
            .attr('x',25 )
            .attr('y', 25 )
            .attr('width', `${width - 50}` )
            .attr('height', `${height - 50}` )
            .attr('rx', '15');

        g_new.append( 'image' )
            .attr('x', 5 )
            .attr('mask', 'url(#testmask)')
            .attr('href', `${content[position].img}` );

        g_new.append('text')
            .attr('class', 'sh_title')
            .attr('x', width / 2 )
            .attr('y', 0 )
            .text(`${content[ position].title}`);

        // Slide elements across one whole page
        //mark current as old, new as current.

        let trn = d3.active( this )
          .transition()
            .duration( `${transit}` )
            .attr('transform', `translate( -${width}, 0)`)
            .delay( `${delay}` );

        if( delay > 0 ) trn.on( 'start', slide );

        //modify control layer
        controls.selectAll('rect').each( function(d,i){
            d3.select( this )
                .attr('fill', function() {
                    if( this.id == position ) return 'orange';
                    return 'gray';
                });
        });
    }

    /* Alter Properties
     * ------------------------- */
    slideshow.width = function ( value ) {
        if(! arguments.length) { return width; }
        width = value;
        return slideshow;
    }
    slideshow.height = function( value ) {
        if(! arguments.length) { return height; }
        height = value;
        return slideshow;
    }
    slideshow.delay = function( value ) {
        if(! arguments.length) { return delay; }
        delay = value;
        return slideshow;
    }
    slideshow.transit = function( value ) {
        if(! arguments.length) { return transit; }
        transit = value;
        return slideshow;
    }

    /* Control Functions
     * ----------------- */

    //trigger a transition
    slideshow.transition = function() {
        slider.transition()
            .duration( 500 )
            .attr('transform', `translate( -${width}, 0)`)
            .on('start', slide );
    }

    // skip to specific slide
    slideshow.skipTo = function( value ){
        position = value -1;

        slideshow.transition();

    }

    return slideshow;
}


